FROM mcr.microsoft.com/dotnet/core/runtime:3.1-buster-slim AS base
WORKDIR /app

FROM mcr.microsoft.com/dotnet/core/sdk:3.1-buster AS build
WORKDIR /src
COPY ["AddressGenerator.csproj", ""]
RUN dotnet restore "./AddressGenerator.csproj"
COPY . .
WORKDIR "/src/."
RUN dotnet build "AddressGenerator.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "AddressGenerator.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "AddressGenerator.dll"]