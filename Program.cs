﻿using NBitcoin;
using System;

namespace AddressGenerator
{
    class Program
    {
        static void Main(string[] args)
        {
            //just a demo on how we can print out root key, derived keys, etc.  All we want in this case is the xpub for account 0 external
            /*
            var n = new Mnemonic("example primary detail pear road comic junk napkin suffer worth index seek position knife rabbit");
            var rootKey = n.DeriveExtKey();
            Console.WriteLine($"Root private key is {rootKey.GetWif(Network.Main)}");
            Console.WriteLine($"Root public key is {rootKey.Neuter().GetWif(Network.Main)}");

            var externalKey = rootKey.Derive(new KeyPath("m/44'/0'/0'/0"));
            Console.WriteLine($"Account 0 external addresses private key is {externalKey.GetWif(Network.Main)}");
            Console.WriteLine($"Account 0 external addresses public key is {externalKey.Neuter().GetWif(Network.Main)}");
            */


            //starting over, let's simply get the Account 0 external addresses public key and generate 100 addresses from it in 1 line.
            var xpub = new Mnemonic("example primary detail pear road comic junk napkin suffer worth index seek position knife rabbit")
                .DeriveExtKey() //this gets the xprv
                .Derive(new KeyPath("m/44'/0'/0'/0")) //per bip44 spec this is bitcoin, account 0, external addresses (this is what a wallet would be generating)
                .Neuter(); //get the xpub

            //really this xpub we have now is all that should be saved.  From it you can generate addresses all day, and even if someone
            //got this xpub they couldn't do anything except generate addresses - no private key in on it.
            Console.WriteLine(xpub.GetWif(Network.Main));
            //100 addresses
            for (uint i = 0; i < 100; i++)
            {
                Console.WriteLine(xpub.Derive(i).PubKey.GetAddress(ScriptPubKeyType.Legacy, Network.Main));
            }
        }
    }
}
